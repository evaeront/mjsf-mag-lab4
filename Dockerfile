FROM node:lts-alpine
RUN apk add --no-cache bash
RUN npm install -g @vue/cli
WORKDIR /usr/src/app


EXPOSE 8080
